# Introduction

#Setup:
Note: Add the libraries in the libs folder to the project. These are needed for the JUnit Tests.


#Task

The purpose of this exercise is to demonstrate your ability to use 
Java to build a dynamic football league table generator.

Consider a league table for football. Each team plays a number of matches and the results
of each match build the table. Given the code attached as a starting point build
the LeagueTable.LeagueTable class that can take a list of completed matches and produce a sorted 
list of LeagueTable.LeagueTableEntry objects.

The sorting rules for entries in the league table should be
* Sort by total points (descending) 
(A win is worth three points to the winning team. A draw is worth one point to each team.)
* Then by goal difference (descending)
* Then by goals scored (descending)
* Then by team name (in alphabetical order)


Your code will be run through a series of JUnit tests to validate the implementation so it is important 
that method signatures are not changed. You will also be assessed on code quality and clarity.

In undertaking this task, please consider the following:
* You should be submitting production quality code
* Future reuse and extension of code
* Any documentation / notes on build


Table ref: https://www.soccerstats.com/homeaway.asp?league=england