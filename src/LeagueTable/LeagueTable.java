package LeagueTable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LeagueTable
{
    private List<Match> matches;

    public LeagueTable( final List<Match> matches )
    {
        this.matches = matches;
    }

    /**
     * Get the ordered list of league table entries for this league table.
     *
     * @return
     */
    public List<LeagueTableEntry> getTableEntriesByPoints()
    {
        List<LeagueTableEntry> leagueTableEntries = new ArrayList<>();
        createLeagueTableEntries(leagueTableEntries);
        leagueTableEntries.sort(Comparator.comparing(LeagueTableEntry::getPoints)
                .reversed());
        return leagueTableEntries;
    }

    public List<LeagueTableEntry> getTableEntriesByGoalDifference()
    {
        List<LeagueTableEntry> leagueTableEntries = new ArrayList<>();
        createLeagueTableEntries(leagueTableEntries);
        leagueTableEntries.sort(Comparator.comparing(LeagueTableEntry::getGoalDifference)
                .reversed());
        return leagueTableEntries;
    }

    public List<LeagueTableEntry> getTableEntriesByGoals()
    {
        List<LeagueTableEntry> leagueTableEntries = new ArrayList<>();
        createLeagueTableEntries(leagueTableEntries);
        leagueTableEntries.sort(Comparator.comparing(LeagueTableEntry::getGoalsFor)
                .reversed());
        return leagueTableEntries;
    }

    public List<LeagueTableEntry> getTableEntriesByTeamName()
    {
        List<LeagueTableEntry> leagueTableEntries = new ArrayList<>();
        createLeagueTableEntries(leagueTableEntries);
        leagueTableEntries.sort(Comparator.comparing(LeagueTableEntry::getTeamName));
        return leagueTableEntries;
    }

    private void createLeagueTableEntries(List<LeagueTableEntry> leagueTableEntries) {
        for (Match match : matches) {

            LeagueTableEntry leagueTableEntryHome = new LeagueTableEntry(match.getHomeTeam(), 0,0,0,0,
                    match.getHomeScore(), match.getAwayScore(), match.getGoalDifference(), 0);
            LeagueTableEntry leagueTableEntryAway = new LeagueTableEntry(match.getAwayTeam(), 0,0,0,0,
                    match.getAwayScore(), match.getHomeScore(), match.getGoalDifference(), 0);

            leagueTableEntries.add(leagueTableEntryHome);
            leagueTableEntries.add(leagueTableEntryAway);
        }
    }
}
