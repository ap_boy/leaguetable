package test;

import LeagueTable.LeagueTable;
import LeagueTable.LeagueTableEntry;
import LeagueTable.Match;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;

public class test {

    @Test
    void getTableEntries_ReturnsPointsInDescendingOrder() {
        Match match1 = new Match("Liverpool", "Manchester", 0, 0);
        Match match2 = new Match("Chelsea", "NewCastle", 0, 0);
        Match match3 = new Match("Aresenal", "Westlake", 0, 0);
        List<Match> matches = new ArrayList<>();
        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        LeagueTable leagueTable = new LeagueTable(matches);
        List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntriesByPoints();

        Assert.assertThat(leagueTableEntries.get(leagueTableEntries.size()-1).getPoints(), is(0));
    }

    @Test
    void getTableEntries_WhenInitialisedWithNoScore_ReturnsTeamNamesInAlphabeticalOrder() {
        Match match1 = new Match("Liverpool", "Manchester", 0, 0);
        Match match2 = new Match("Chelsea", "NewCastle", 0, 0);
        Match match3 = new Match("Aresenal", "Westlake", 0, 0);
        List<Match> matches = new ArrayList<>();
        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        LeagueTable leagueTable = new LeagueTable(matches);
        List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntriesByTeamName();

        Assert.assertThat(leagueTableEntries.get(leagueTableEntries.size()-1).getTeamName(), is("Westlake"));
    }

    @Test
    void getTableEntries_ReturnsHomeScoresInDescendingOrder() {
        Match match1 = new Match("", "", 10, 8);
        Match match2 = new Match("", "", 3, 7);
        Match match3 = new Match("", "", 15, 1);
        List<Match> matches = new ArrayList<>();
        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        LeagueTable leagueTable = new LeagueTable(matches);
        List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntriesByGoals();

        Assert.assertThat(leagueTableEntries.get(0).getGoalsFor(), is(15));
    }

    @Test
    void getTableEntries_ReturnsHomeGoalDifferenceInDescendingOrder() {
        Match match1 = new Match("Liverpool", "Manchester", 10, 8);
        Match match2 = new Match("Chelsea", "NewCastle", 3, 7);
        Match match3 = new Match("Aresenal", "Westlake", 15, 1);
        List<Match> matches = new ArrayList<>();
        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        LeagueTable leagueTable = new LeagueTable(matches);
        List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntriesByGoalDifference();

        Assert.assertThat(leagueTableEntries.get(0).getGoalDifference(), is(14));
    }
}